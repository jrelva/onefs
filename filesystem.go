package OneFS



import (
	"strconv"
	"log"
	"crypto/tls"
	"net/http"
	"fmt"
	"net/http/cookiejar"
	"bufio"
	"encoding/json"
	"bytes"
	"io/ioutil"
	"os"

)

type IsilonConfiguration struct {
	Username string
	Password string
	Hostname string
	Port int
	Services []string
	Namespace string
	Session *http.Client
}

// Authorise with the Isilon API
// Get the authorisation cookie and store it with the client
func CreateSession(c *IsilonConfiguration) {
	type AuthResponse struct {
		Services []string `json:"services"`
		Username string `json:"username"`
		TimeoutAbsolute int `json:"timeout_absolute"`
		TimeoutInactive int `json:"timeout_inactive"`
	}

	type AuthPayload struct {
		Services []string `json:"services"`
		Username string `json:"username"`
		Password string `json:"password"`
	}

	type AuthError struct {
		Message string
	}

	// Add a cookie jar to save authentication cookie
	cookieJar, _ := cookiejar.New(nil)

	// Hack to allow unverified SSL
	tr := &http.Transport{TLSClientConfig:&tls.Config{InsecureSkipVerify:true}}
	s := &http.Client{Transport:tr, Jar: cookieJar}


	payload, _ := json.Marshal(AuthPayload{Services:c.Services, Username:c.Username, Password:c.Password})

	url := "https://" + c.Hostname + ":" + strconv.Itoa(c.Port) + "/session/1/session"

	req, _ := http.NewRequest("POST", url, bytes.NewBuffer(payload))
	req.Header.Set("Content-Type", "application/json")

	resp, err := s.Do(req)
	if err != nil {
		log.Fatal(err)
	}

	if resp.StatusCode > 300 {
		body, _ := ioutil.ReadAll(resp.Body)
		defer resp.Body.Close()

		var e AuthError

		json.Unmarshal([]byte(body), &e)

		log.Fatal(e.Message)
	}


	c.Session = s

}

type APIError struct {
	Errors []map[string]string `json:"errors"`
}

// GET a namespace within the Isilon API and return a http.Response object
func Fs(i *IsilonConfiguration, d *string) (*http.Response, error) {
	url := "https://" + i.Hostname + ":" + strconv.Itoa(i.Port) + "/namespace" + i.Namespace

	url += *d

	resp, _ := i.Session.Get(url)

	return resp, nil
}


// GET metadata for a given namespace
func Meta(i *IsilonConfiguration, d *string) (*map[string]interface{}, error) {
	url := "https://" + i.Hostname + ":" + strconv.Itoa(i.Port) + "/namespace" + i.Namespace
	url += *d
	url += "?metadata"

	type Metadata struct {
		Attrs []map[string]interface{} `json:"attrs"`
	}

	res := Metadata{}
	e := APIError{}

	resp, err := i.Session.Get(url)
	body, _ := ioutil.ReadAll(resp.Body)

	defer resp.Body.Close()

	if resp.StatusCode >  300 {
		json.Unmarshal(body, &e)
		for _, em := range e.Errors {
			log.Fatal(em["code"] + ": " + em["message"])
		}
	}

	json.Unmarshal(body, &res)

	if err != nil {
		log.Fatal(err)
	}


	meta := make(map[string]interface{})
	for _,m := range res.Attrs {
		k := m["name"].(string)
		meta[k] = m["value"]
	}



	return &meta, nil
}

func ListDirectory(c *IsilonConfiguration, d *string) {
	resp, _ := Fs(c, d)

	defer resp.Body.Close()


	type FileSystem struct {
		Children []map[string]string `json:"children"`
	}

	children := FileSystem{}

	body, _ := ioutil.ReadAll(resp.Body)

	err := json.Unmarshal([]byte(body), &children)

	if err != nil {
		log.Fatal("Not a directory")
	}

	for _, child := range children.Children {
		fmt.Println(child["name"])
	}
}


func StreamObject(c *IsilonConfiguration, d *string) *bufio.Reader {
	resp, _ := Fs(c, d)
	return bufio.NewReader(resp.Body)

}

func DownloadFile(c *IsilonConfiguration, path *string, output *string, bufsize *int) {
	stream := StreamObject(c, path)
	buf := make([]byte, *bufsize)

	var f os.File

	if *output == "" {
		f = *os.Stdout
	} else {
		of, _ := os.Create(*output)
		f = *of
	}

	defer f.Close()

	for {
		v, _ := stream.Read(buf)

		if v == 0 {
			break
		}

		f.Write(buf)
	}
}